mod convert;
mod editor;
mod net;
mod pos;

use std::{num::NonZeroU32, process::ExitCode};

use editor::Editor;
use femtovg::{Canvas, renderer::OpenGl};
use glutin::{
    config::ConfigTemplateBuilder,
    context::{ContextApi, ContextAttributesBuilder},
    display::GetGlDisplay,
    prelude::*,
    surface::{SurfaceAttributesBuilder, WindowSurface},
};
use glutin_winit::DisplayBuilder;
use raw_window_handle::HasRawWindowHandle;
use resource::resource;
use winit::{event::ModifiersState, event_loop::EventLoop, window::WindowBuilder};

fn main() -> ExitCode {
    let w = 16 * 80;
    let h = 9 * 80;

    let event_loop = EventLoop::new();

    let (renderer, window, context, surface) = {
        let window_builder = WindowBuilder::new()
            .with_inner_size(winit::dpi::PhysicalSize::new(w, h))
            .with_title("Flow Editor");

        let template = ConfigTemplateBuilder::new().with_alpha_size(8);

        let display_builder = DisplayBuilder::new().with_window_builder(Some(window_builder));

        let (window, gl_config) = display_builder
            .build(&event_loop, template, |configs| {
                configs
                    .reduce(|accum, config| {
                        let transparency_check = config.supports_transparency().unwrap_or(false)
                            & !accum.supports_transparency().unwrap_or(false);

                        if transparency_check || config.num_samples() > accum.num_samples() {
                            config
                        } else {
                            accum
                        }
                    })
                    .expect("Display configuragion error")
            })
            .expect("Display build error");

        let window = window.expect("Window configuration error");

        let raw_window_handle = Some(window.raw_window_handle());

        let gl_display = gl_config.display();

        let context_attributes = ContextAttributesBuilder::new().build(raw_window_handle);
        let fallback_context_attributes = ContextAttributesBuilder::new()
            .with_context_api(ContextApi::Gles(None))
            .build(raw_window_handle);
        let mut not_current_gl_context = Some(unsafe {
            gl_display
                .create_context(&gl_config, &context_attributes)
                .unwrap_or_else(|_| {
                    gl_display
                        .create_context(&gl_config, &fallback_context_attributes)
                        .expect("Failed to create context")
                })
        });

        let (width, height): (u32, u32) = window.inner_size().into();
        let raw_window_handle = window.raw_window_handle();
        let attrs = SurfaceAttributesBuilder::<WindowSurface>::new().build(
            raw_window_handle,
            NonZeroU32::new(width).expect("Width shouldn't be zero"),
            NonZeroU32::new(height).expect("Height shouldn't be zero"),
        );

        let surface = unsafe {
            gl_config
                .display()
                .create_window_surface(&gl_config, &attrs)
                .expect("Surface configuration error")
        };

        let gl_context = not_current_gl_context
            .take()
            .expect("No context available")
            .make_current(&surface)
            .expect("Context configuration error");

        let renderer =
            unsafe { OpenGl::new_from_function_cstr(|s| gl_display.get_proc_address(s).cast()) }
                .expect("Cannot create renderer");

        (renderer, window, gl_context, surface)
    };

    let mut canvas = Canvas::new(renderer).expect("Cannot create canvas");

    canvas.set_size(w, h, 1.0);
    canvas.translate(w as f32 / 2.0, h as f32 / 2.0);

    let mut args = std::env::args();
    args.next();
    let editor = args
        .next()
        .map_or_else(Editor::new, |path| Editor::from_path(path.into()));

    canvas
        .add_font_mem(&resource!("resources/DejaVuSans.ttf"))
        .expect("Cannot add font");

    gapp_winit::run(
        editor,
        event_loop,
        60,
        context,
        surface,
        window,
        ModifiersState::empty(),
        canvas,
    );

    ExitCode::SUCCESS
}
