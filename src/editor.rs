use crate::{convert::Convert as _, net::EditNet, pos::Pos};

use data_stream::{default_settings::PortableSettings, from_stream, to_stream};
use femtovg::{Canvas, LineCap, Paint, Path, Renderer};
use flow_net::{Net, NetDefinition};
use gapp::{Render, Update};
use gapp_winit::WindowInput;
use num_traits::Zero;
use rfd::{FileDialog, MessageButtons, MessageDialog, MessageLevel};
use simple_color::Color;
use simple_vectors::Vector;
use simple_view::View;
use single_whitespace_text_editing::SingleWhitespaceEditAction;
use text_editing::{Direction, TextLine};
use touch_selection::{Action, GrabState, ObjectGrabState, Selection, Selector};
use vector_space::distance;
use winit::{
    event::{
        ElementState, KeyboardInput, ModifiersState, MouseButton, Touch, TouchPhase,
        VirtualKeyCode, WindowEvent,
    },
    event_loop::ControlFlow,
};

use std::{collections::HashSet, fs::File, io::BufReader, path::PathBuf};

struct TouchInfo {
    main: Option<u64>,
    zoomer: Option<u64>,
    distance: f32,
}

struct SelectionEditor {
    snapping: Option<i8>,
}

fn snap(pos: Vector<f32, 2>, f: f32) -> Vector<f32, 2> {
    let x = pos[0];
    let y = pos[1];
    Vector::new([(x / f).round(), (y / f).round()]) * f
}

impl Selector for SelectionEditor {
    type Selectable = EditNet;
    type Position = Vector<f32, 2>;

    fn moved(&self, moved: Pos, cursor: Pos) -> bool {
        distance(moved, cursor) >= 0x10 as f32
    }

    fn stop(&self, selectable: &mut Self::Selectable, index: usize) {
        if let Some(snapping) = self.snapping {
            let pos = &mut selectable.infos[index].pos;
            *pos = snap(*pos, 2.0f32.powi(snapping as i32));
        }
    }
}

impl SelectionEditor {
    fn new() -> Self {
        Self { snapping: Some(3) }
    }
}

pub struct Editor {
    cursor: Pos,
    size: Pos,
    view: View<Pos>,
    selection: Selection<Pos, HashSet<usize>>,
    selection_editor: SelectionEditor,
    touch: TouchInfo,
    path: Option<PathBuf>,
    export_path: Option<PathBuf>,
    net: EditNet,
}

fn create_file_failed(path: &PathBuf) {
    MessageDialog::new()
        .set_level(MessageLevel::Error)
        .set_title("Creating file failed!")
        .set_description(&format!("The file {path:?} could not be created")[..])
        .set_buttons(MessageButtons::Ok)
        .show();
}

fn open_file_failed(path: &PathBuf) {
    MessageDialog::new()
        .set_level(MessageLevel::Error)
        .set_title("Opening file failed!")
        .set_description(&format!("The file {path:?} could not be opened")[..])
        .set_buttons(MessageButtons::Ok)
        .show();
}

impl Editor {
    pub fn new() -> Self {
        Self {
            cursor: Pos::zero(),
            size: Pos::new([1.0, 1.0]),
            view: View {
                zoom: 1.0 / 512.0,
                offset: Pos::zero(),
            },
            selection: Selection::default(),
            selection_editor: SelectionEditor::new(),
            touch: TouchInfo {
                main: None,
                zoomer: None,
                distance: 0.0,
            },
            path: None,
            export_path: None,
            net: EditNet::new(Net::new()),
        }
    }

    pub fn from_path(path: PathBuf) -> Self {
        let mut result = Self::new();

        result.path = Some(path);
        result.load_current_path();

        result
    }

    pub fn text_input(&mut self, character: char, skip: bool) {
        let Some(edit_action) = SingleWhitespaceEditAction::new(character, skip) else {
            return;
        };

        for &selected in &self.selection {
            let selected_info = &mut self.net.infos[selected];
            edit_action.apply(&mut selected_info.name, &mut selected_info.name_cursor);
        }
    }

    pub fn move_text_cursor(&mut self, direction: Direction, skip: bool) {
        let call = TextLine::cursor_movement(direction, skip);

        for &selected in &self.selection {
            let node = &mut self.net.infos[selected];
            call(&node.name, &mut node.name_cursor);
        }
    }

    fn file_dialog(&self) -> FileDialog {
        let dialog = FileDialog::new()
            .add_filter("Flow nets", &["flow"])
            .add_filter("Path definition", &["paths"]);

        let mut path = if let Some(path) = &self.path {
            path.clone()
        } else if let Ok(path) = std::env::current_dir() {
            path
        } else {
            return dialog;
        };

        if path.is_file() {
            if let Some(parent) = path.parent() {
                path = parent.to_path_buf();
            }
        }

        dialog.set_directory(path)
    }

    fn export_file_dialog(&self) -> FileDialog {
        let dialog = FileDialog::new().add_filter("GraphViz dot files", &["dot"]);

        let mut path = if let Some(path) = &self.export_path {
            path.clone()
        } else if let Ok(path) = std::env::current_dir() {
            path
        } else {
            return dialog;
        };

        if path.is_file() {
            if let Some(parent) = path.parent() {
                path = parent.to_path_buf();
            }
        }

        dialog.set_directory(path)
    }

    fn save_current_path(&self) {
        let path = self.path.as_ref().unwrap();
        let Some(extension) = path.extension() else {
            MessageDialog::new()
                .set_level(MessageLevel::Error)
                .set_title("Saving failed!")
                .set_description(
                    &"Filename needs to have a supported extension (\"flow\", \"paths\")"
                        .to_string()[..],
                )
                .set_buttons(MessageButtons::Ok)
                .show();
            return;
        };

        let Some(extension) = extension.to_str() else {
            MessageDialog::new()
                .set_level(MessageLevel::Error)
                .set_title("Saving failed!")
                .set_description(&"Invalid file extension".to_string()[..])
                .set_buttons(MessageButtons::Ok)
                .show();
            return;
        };

        match extension {
            "flow" => {
                let Ok(mut file) = File::create(path) else {
                    create_file_failed(path);
                    return;
                };

                let definition = self.net.net.to_definition();
                let _ = to_stream::<PortableSettings, _, _>(&definition, &mut file);
            }
            "paths" => {
                let Ok(mut file) = File::create(path) else {
                    create_file_failed(path);
                    return;
                };

                let definition = self.net.net.to_definition();
                let names: Vec<String> = self
                    .net
                    .infos
                    .iter()
                    .map(|info| info.name.to_string())
                    .collect();
                definition.to_lines(&names[..], &mut file);
            }
            _ => {
                MessageDialog::new()
                    .set_level(MessageLevel::Error)
                    .set_title("Saving failed!")
                    .set_description(
                        &format!("Unsupported file extension: {extension} (supported: \"flow\", \"paths\")")[..],
                    )
                    .set_buttons(MessageButtons::Ok)
                    .show();
            }
        }
    }

    fn save(&mut self, force_path: bool) {
        if force_path || self.path.is_none() {
            let dialog = self.file_dialog();

            let Some(path) = dialog.save_file() else {
                return;
            };

            self.path = Some(path)
        }

        self.save_current_path();
    }

    fn load_current_path(&mut self) {
        let path = self.path.as_ref().unwrap();
        let Some(extension) = path.extension() else {
            MessageDialog::new()
                .set_level(MessageLevel::Error)
                .set_title("Loading failed!")
                .set_description(
                    &"Filename needs to have a supported extension (\"flow\", \"paths\")"
                        .to_string()[..],
                )
                .set_buttons(MessageButtons::Ok)
                .show();
            return;
        };

        let Some(extension) = extension.to_str() else {
            MessageDialog::new()
                .set_level(MessageLevel::Error)
                .set_title("Loading failed!")
                .set_description(&"Invalid file extension".to_string()[..])
                .set_buttons(MessageButtons::Ok)
                .show();
            return;
        };

        match extension {
            "flow" => {
                let Ok(mut file) = File::open(path) else {
                    open_file_failed(path);
                    return;
                };

                if let Ok(definition) = from_stream::<PortableSettings, _, _>(&mut file) {
                    self.net = EditNet::new(Net::from_definition(&definition));
                }
            }
            "paths" => {
                let Ok(file) = File::open(path) else {
                    open_file_failed(path);
                    return;
                };

                let mut reader = BufReader::new(file);
                if let Ok((definition, names)) = NetDefinition::from_lines(&mut reader) {
                    self.net = EditNet::with_names(Net::from_definition(&definition), names);
                }
            }
            _ => {
                MessageDialog::new()
                    .set_level(MessageLevel::Error)
                    .set_title("Loading failed!")
                    .set_description(
                        &format!("Unsupported file extension: {extension} (supported: \"flow\", \"paths\")")[..],
                    )
                    .set_buttons(MessageButtons::Ok)
                    .show();
            }
        }
    }

    fn load(&mut self, force_path: bool) {
        if force_path || self.path.is_none() {
            let dialog = self.file_dialog();

            let Some(path) = dialog.pick_file() else {
                return;
            };

            self.path = Some(path)
        }

        self.load_current_path();
    }

    fn export_current_path(&self) {
        let path = self.export_path.as_ref().unwrap();
        let Some(extension) = path.extension() else {
            MessageDialog::new()
                .set_level(MessageLevel::Error)
                .set_title("Export failed!")
                .set_description(
                    &"Filename needs to have a supported extension (\"dot\")".to_string()[..],
                )
                .set_buttons(MessageButtons::Ok)
                .show();
            return;
        };

        let Some(extension) = extension.to_str() else {
            MessageDialog::new()
                .set_level(MessageLevel::Error)
                .set_title("Export failed!")
                .set_description(&"Invalid file extension".to_string()[..])
                .set_buttons(MessageButtons::Ok)
                .show();
            return;
        };

        match extension {
            "dot" => {
                let Ok(mut file) = File::create(path) else {
                    create_file_failed(path);
                    return;
                };

                let names: Vec<_> = self
                    .net
                    .infos
                    .iter()
                    .map(|info| info.name.to_string())
                    .collect();
                self.net.net.to_dot(&names[..], &mut file);
            }
            _ => {
                MessageDialog::new()
                    .set_level(MessageLevel::Error)
                    .set_title("Export failed!")
                    .set_description(
                        &format!("Unsupported file extension: {extension} (supported: \"dot\")")[..],
                    )
                    .set_buttons(MessageButtons::Ok)
                    .show();
            }
        }
    }

    fn export(&mut self, force_path: bool) {
        if force_path || self.export_path.is_none() {
            let dialog = self.export_file_dialog();

            let Some(path) = dialog.save_file() else {
                return;
            };

            self.export_path = Some(path)
        }

        self.export_current_path();
    }
}

impl Update for Editor {
    fn update(&mut self, timestep: f32) {
        let Some(grab) = &mut self.selection.grab else {
            return;
        };

        if grab.moved.is_some() {
            grab.time += timestep;
            if grab.time >= 0.3 {
                grab.hold(&self.selection_editor, &mut self.net);
            }
            return;
        }

        use GrabState::*;
        let Object(ObjectGrabState { action, .. }) = grab.state else {
            self.view.offset += (self.view.unclip(self.cursor) - grab.pos) * self.view.zoom;
            return;
        };

        if matches!(action, Action::Move) {
            let grab_pos0 = grab.pos;
            grab.pos = self.view.unclip(self.cursor);
            let grab_pos1 = grab.pos;
            for &event in &self.selection {
                let info = &mut self.net.infos[event];
                let grab_offset = info.pos - grab_pos0;
                info.pos = grab_pos1 + grab_offset
            }
        }

        let min_size = self.size[0].min(self.size[1]);
        let border_size = min_size / 4.0;

        let max = self.size / 2.0 - Pos::new([border_size; 2]);

        let x_ratio = if self.cursor[0] > max[0] {
            max[0] - self.cursor[0]
        } else if self.cursor[0] < -max[0] {
            -max[0] - self.cursor[0]
        } else {
            0.0
        } / border_size;

        let y_ratio = if self.cursor[1] > max[1] {
            max[1] - self.cursor[1]
        } else if self.cursor[1] < -max[1] {
            -max[1] - self.cursor[1]
        } else {
            0.0
        } / border_size;

        self.view.offset += Pos::new([x_ratio * x_ratio.abs(), y_ratio * y_ratio.abs()])
            * (self.view.zoom * 0x400 as f32 * timestep);
    }
}

impl<R: Renderer> Render<Canvas<R>> for Editor {
    fn render(&self, canvas: &mut Canvas<R>) {
        canvas.clear_rect(
            0,
            0,
            canvas.width(),
            canvas.height(),
            Color::gray(0x40).convert(),
        );

        self.net
            .render(canvas, &self.view, &self.selection.selected);

        let Some(grab_info) = &self.selection.grab else {
            canvas.flush();
            return;
        };

        if grab_info.moved.is_some() {
            canvas.flush();
            return;
        }

        let &GrabState::Object(ObjectGrabState {
            action: Action::Connect,
            index: grab_event,
            ..
        }) = &grab_info.state
        else {
            canvas.flush();
            return;
        };

        let grab_event_info = &self.net.infos[grab_event];
        let grab_offset = grab_info.pos - grab_event_info.pos;
        for &event in &self.selection {
            let event_info = &self.net.infos[event];
            let clipped = self.view.clip(Pos::new([
                event_info.pos[0] + grab_offset[0],
                event_info.pos[1] + 8.0,
            ]));

            let [sx, sy]: [f32; 2] = clipped.into();
            let [ex, ey]: [f32; 2] = self.cursor.into();

            let line_paint = Paint::color(Color::gray(128).convert())
                .with_line_cap(LineCap::Butt)
                .with_line_width(2.0 * self.view.zoom);

            let mut line = Path::new();
            line.move_to(sx, sy);

            let mut vertical_offset = (sx - ex).abs().sqrt() * 8.0 * self.view.zoom.sqrt();
            if ey < sy {
                vertical_offset += (sy - ey).sqrt() * 8.0 * self.view.zoom.sqrt();
            }
            line.bezier_to(sx, sy + vertical_offset, ex, ey - vertical_offset, ex, ey);

            canvas.stroke_path(&line, &line_paint);

            let mut arrow = Path::new();
            arrow.move_to(ex, ey + self.view.zoom * 2.0);
            arrow.line_to(ex - self.view.zoom * 2.0, ey - self.view.zoom * 2.0);
            arrow.line_to(ex + self.view.zoom * 2.0, ey - self.view.zoom * 2.0);
            arrow.line_to(ex, ey + self.view.zoom * 2.0);

            canvas.fill_path(&arrow, &line_paint);
        }

        canvas.flush();
    }
}

impl<R: Renderer> WindowInput<ModifiersState, Canvas<R>> for Editor {
    fn input(
        &mut self,
        event: &WindowEvent<'_>,
        control_flow: &mut ControlFlow,
        modifiers: &mut ModifiersState,
        canvas: &mut Canvas<R>,
    ) {
        match event {
            &WindowEvent::ModifiersChanged(new_modifiers) => *modifiers = new_modifiers,

            WindowEvent::CloseRequested => *control_flow = ControlFlow::Exit,

            WindowEvent::Resized(size) => {
                let [w, h] = [size.width, size.height];

                canvas.set_size(w, h, 1.0);
                canvas.reset_transform();
                canvas.translate(w as f32 / 2.0, h as f32 / 2.0);

                let divisor = self.size[0].min(self.size[1]);
                self.size = Pos::new([w as f32, h as f32]);
                let factor = self.size[0].min(self.size[1]);
                self.view.zoom(factor / divisor, Pos::new([0.0, 0.0]));
            }

            WindowEvent::MouseWheel {
                delta: winit::event::MouseScrollDelta::LineDelta(_, roll),
                ..
            } => self.view.zoom(2.0f32.powf(roll / 0x10 as f32), self.cursor),

            WindowEvent::CursorMoved { position, .. } => {
                self.cursor = Pos::new([position.x as f32, position.y as f32]) - self.size / 2.0;
                self.selection.on_moved(self.cursor, &self.selection_editor)
            }

            WindowEvent::KeyboardInput {
                input:
                    KeyboardInput {
                        virtual_keycode: Some(keycode),
                        state: ElementState::Pressed,
                        ..
                    },
                ..
            } => {
                match keycode {
                    VirtualKeyCode::Left => {
                        self.move_text_cursor(Direction::Backward, modifiers.ctrl())
                    }
                    VirtualKeyCode::Right => {
                        self.move_text_cursor(Direction::Forward, modifiers.ctrl())
                    }
                    _ => (),
                }

                if !modifiers.ctrl() {
                    return;
                }

                match keycode {
                    VirtualKeyCode::R => {
                        self.net = EditNet::new(std::mem::replace(&mut self.net.net, Net::new()));
                    }
                    VirtualKeyCode::S => self.save(modifiers.shift()),
                    VirtualKeyCode::E => self.export(modifiers.shift()),
                    VirtualKeyCode::O => self.load(!modifiers.shift()),
                    _ => (),
                }
            }

            &WindowEvent::Touch(Touch {
                phase,
                location,
                id,
                ..
            }) => {
                if let Some(current) = self.touch.main {
                    if current == id {
                        self.cursor =
                            Pos::new([location.x as f32, location.y as f32]) - self.size / 2.0;
                        self.selection.on_moved(self.cursor, &self.selection_editor);

                        if phase == TouchPhase::Ended {
                            self.selection.release(
                                self.view.unclip(self.cursor),
                                &self.selection_editor,
                                &mut self.net,
                            );
                            self.touch.main = None;
                        }
                    }
                } else if phase == TouchPhase::Started {
                    self.selection.press(
                        self.view.unclip(self.cursor),
                        self.cursor,
                        &self.selection_editor,
                        &self.net,
                    );
                    self.touch.main = Some(id);
                }

                let zoom_pos = Pos::new([location.x as f32, location.y as f32]) - self.size / 2.0;
                let dis = distance(zoom_pos, self.cursor);

                if dis.is_zero() {
                    return;
                }

                if let Some(zoomer) = self.touch.zoomer {
                    if zoomer == id {
                        self.view.zoom(dis / self.touch.distance, self.cursor);
                        self.touch.distance = dis;

                        if phase == TouchPhase::Ended {
                            self.touch.zoomer = None;
                        }
                    }
                } else if phase == TouchPhase::Started {
                    self.touch.distance = dis;
                    self.touch.zoomer = Some(id);
                }
            }

            &WindowEvent::ReceivedCharacter(c) => self.text_input(c, modifiers.ctrl()),

            WindowEvent::MouseInput { state, button, .. } => {
                let pos = self.view.unclip(self.cursor);

                use ElementState::*;
                match state {
                    Pressed => {
                        use MouseButton::*;

                        if matches!(button, Left | Right) {
                            self.selection.press(
                                pos,
                                self.cursor,
                                &self.selection_editor,
                                &self.net,
                            );
                        }

                        if matches!(button, Right) {
                            self.selection
                                .grab
                                .as_mut()
                                .unwrap()
                                .hold(&self.selection_editor, &mut self.net);
                        }
                    }

                    Released => self
                        .selection
                        .release(pos, &self.selection_editor, &mut self.net),
                }
            }

            _ => (),
        }
    }
}
