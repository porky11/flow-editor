use crate::{convert::Convert as _, pos::Pos};

use femtovg::{Align, Baseline, Canvas, LineCap, Paint, Path, Renderer};
use flow_net::Net;
use simple_color::Color;
use simple_femto_text_rendering::{CursorSettings, render_text};
use simple_view::View;
use text_editing::TextLine;
use touch_selection::Selectable;

const SIZE: Pos = Pos::new([0x40 as f32, 0x10 as f32]);

use std::collections::{HashMap, HashSet};

pub struct EventInfo {
    pub pos: Pos,
    pub name: TextLine,
    pub name_cursor: usize,
}

impl EventInfo {
    fn new(pos: Pos, name: String) -> Self {
        let name = TextLine::from_string(name);
        let name_cursor = name.len();
        Self {
            pos,
            name,
            name_cursor,
        }
    }

    pub fn contains(&self, inner: Pos) -> bool {
        let [x, y]: [f32; 2] = self.pos.into();
        let [ix, iy]: [f32; 2] = inner.into();
        let w = SIZE[0] / 2.0;
        let h = SIZE[1] / 2.0;
        x - w <= ix && ix <= x + w && y - h <= iy && iy <= y + h
    }
}

pub struct EditNet {
    pub net: Net,
    pub infos: Vec<EventInfo>,
}

impl EditNet {
    pub fn new(net: Net) -> Self {
        let count = net.events().len();

        Self {
            net,
            infos: (0..count)
                .map(|event| {
                    EventInfo::new(
                        Pos::new([0.0, ((event as f32 * 2.0 - count as f32) * 0x10 as f32)]),
                        format!("Event {}", event + 1),
                    )
                })
                .collect(),
        }
    }

    pub fn with_names<I: IntoIterator<Item = String>>(net: Net, names: I) -> Self {
        let count = net.events().len();
        let mut names = names.into_iter();

        Self {
            net,
            infos: (0..count)
                .map(|event| {
                    EventInfo::new(
                        Pos::new([0.0, ((event as f32 * 2.0 - count as f32) * 0x10 as f32)]),
                        names
                            .next()
                            .unwrap_or_else(|| format!("Event {}", event + 1)),
                    )
                })
                .collect(),
        }
    }

    pub fn render<R: Renderer>(
        &self,
        canvas: &mut Canvas<R>,
        view: &View<Pos>,
        selected: &HashSet<usize>,
    ) {
        let size = SIZE * view.zoom;

        let emitter_nodes = &self.net.emitters();

        let black = Paint::color(Color::BLACK.convert())
            .with_line_width(2.0 * view.zoom)
            .with_font_size(8.0 * view.zoom)
            .with_text_align(Align::Center)
            .with_text_baseline(Baseline::Middle);

        let select_outline = Paint::color(Color::YELLOW.convert()).with_line_width(2.0 * view.zoom);

        let mut emitter_positions = HashMap::new();
        let mut responder_positions = HashMap::new();

        for (event_node, info) in self.net.events().iter().zip(&self.infos) {
            let pos = view.clip(info.pos);

            let responder_count = event_node.responders.len();
            for (i, responder) in event_node.responders.iter().enumerate() {
                let x = pos[0] + ((i + 1) as f32 / (responder_count + 1) as f32 - 0.5) * size[0];
                let y = pos[1] - size[1] / 2.0;

                responder_positions.insert(responder, Pos::new([x, y]));
            }

            let emitter_count = event_node.emitters.len();
            for (i, &emitter) in event_node.emitters.iter().enumerate() {
                let x = pos[0] + ((i + 1) as f32 / (emitter_count + 1) as f32 - 0.5) * size[0];
                let y = pos[1] + size[1] / 2.0;

                emitter_positions.insert(emitter, Pos::new([x, y]));
            }
        }

        for (i, (event_node, info)) in self.net.events().iter().zip(&self.infos).enumerate() {
            let pos = view.clip(info.pos);
            let corner = pos - size / 2.0;

            let mut path = Path::new();
            path.rounded_rect(corner[0], corner[1], size[0], size[1], 4.0 * view.zoom);

            let callable = !event_node.responders.is_empty();

            let select = selected.contains(&i);
            let outline = if select { &select_outline } else { &black };
            canvas.stroke_path(&path, outline);

            let gradient = Paint::box_gradient(
                corner[0] + 2.0 * view.zoom,
                corner[1] + 2.0 * view.zoom,
                size[0] - 4.0 * view.zoom,
                size[1] - 4.0 * view.zoom,
                2.0 * view.zoom,
                4.0 * view.zoom,
                if callable {
                    Color::WHITE
                } else {
                    Color::rgb(255, 128, 128)
                }
                .convert(),
                if select {
                    Color::rgb(128, 128, 0)
                } else {
                    Color::gray(64)
                }
                .convert(),
            );
            canvas.fill_path(&path, &gradient);

            let name = info.name.as_str();
            let [x, y]: [f32; 2] = pos.into();

            render_text(
                name,
                x,
                y,
                size[0],
                size[1] / 2.0,
                &black,
                select.then(|| CursorSettings {
                    width: view.zoom / 2.0,
                    index: info.name.string_index(info.name_cursor),
                }),
                None,
                canvas,
            );
        }

        let line_paint = Paint::color(Color::gray(128).convert())
            .with_line_cap(LineCap::Butt)
            .with_line_width(2.0 * view.zoom);

        for (emitter, start_pos) in emitter_positions {
            let [sx, sy]: [f32; 2] = start_pos.into();

            let emitter_node = &emitter_nodes[emitter];
            for responder in &emitter_node.responders {
                let end_pos = responder_positions[responder];
                let [ex, ey]: [f32; 2] = end_pos.into();

                let mut line = Path::new();
                line.move_to(sx, sy);

                let mut vertical_offset = (sx - ex).abs().sqrt() * 8.0 * view.zoom.sqrt();
                if ey < sy {
                    vertical_offset += (sy - ey).sqrt() * 8.0 * view.zoom.sqrt();
                }
                line.bezier_to(sx, sy + vertical_offset, ex, ey - vertical_offset, ex, ey);

                canvas.stroke_path(&line, &line_paint);

                let mut arrow = Path::new();
                arrow.move_to(ex, ey + view.zoom * 2.0);
                arrow.line_to(ex - view.zoom * 2.0, ey - view.zoom * 2.0);
                arrow.line_to(ex + view.zoom * 2.0, ey - view.zoom * 2.0);
                arrow.line_to(ex, ey + view.zoom * 2.0);

                canvas.fill_path(&arrow, &line_paint);
            }
        }
    }
}

impl Selectable for EditNet {
    type Position = Pos;
    type Index = usize;

    fn add(&mut self, pos: Pos, _from: Option<usize>) -> usize {
        let event = self.net.add_event();
        let event_info = EventInfo::new(pos, format!("Event {}", event + 1));
        if event < self.infos.len() {
            self.infos[event] = event_info;
        } else {
            self.infos.push(event_info);
        }

        event
    }

    fn get(&self, pos: Pos) -> Option<usize> {
        for (index, event_info) in self.infos.iter().enumerate().rev() {
            if event_info.contains(pos) {
                return Some(index);
            }
        }

        None
    }

    fn connect(&mut self, index: usize, selected: &[usize]) {
        self.net.add_path(index, selected);
    }

    fn special(&mut self, index: usize, selected: &[usize]) {
        self.net.make_parallel(index, selected)
    }
}
