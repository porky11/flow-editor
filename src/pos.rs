use simple_vectors::Vector;

pub type Pos = Vector<f32, 2>;
